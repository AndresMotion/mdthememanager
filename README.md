# MDThemeManager

[![CI Status](http://img.shields.io/travis/Andres Rivas/MDThemeManager.svg?style=flat)](https://travis-ci.org/Andres Rivas/MDThemeManager)
[![Version](https://img.shields.io/cocoapods/v/MDThemeManager.svg?style=flat)](http://cocoapods.org/pods/MDThemeManager)
[![License](https://img.shields.io/cocoapods/l/MDThemeManager.svg?style=flat)](http://cocoapods.org/pods/MDThemeManager)
[![Platform](https://img.shields.io/cocoapods/p/MDThemeManager.svg?style=flat)](http://cocoapods.org/pods/MDThemeManager)

## Example

To run the example project, clone the repo, and run `pod install` from the Example directory first.

## Requirements

## Installation

MDThemeManager is available through [CocoaPods](http://cocoapods.org). To install
it, simply add the following line to your Podfile:

```ruby
pod 'MDThemeManager'
```

## Author

Andres Rivas, andres@motiondisplays.com

## License

MDThemeManager is available under the MIT license. See the LICENSE file for more info.
