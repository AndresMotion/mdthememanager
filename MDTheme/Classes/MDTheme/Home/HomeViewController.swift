//
//  HomeViewController.swift
//  MDThemes
//
//  Created by Andrés Alexis Rivas Solorzano on 30-01-18.
//  Copyright © 2018 Andrés Alexis Rivas Solorzano. All rights reserved.
//

import UIKit

class HomeViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationController!.navigationBar.isTranslucent = false
      
        // Do any additional setup after loading the view.
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.navigationController!.isNavigationBarHidden = true
        self.navigationController!.isNavigationBarHidden = false
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func goToSettings(_ sender: UIButton) {
        self.navigationController?.present(MDThemeSettingsViewController(), animated: true)
    }
    
    @IBAction func GoToAssets(_ sender: Any) {
        self.navigationController?.pushViewController(AssetsViewController(), animated: true)
    }
    
    @IBAction func GoToColors(_ sender: Any) {
        
    }
    @IBAction func GoToText(_ sender: Any) {
        self.navigationController?.pushViewController(MainViewController(), animated: true)
     }
    /*
     // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
