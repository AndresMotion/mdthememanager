//
//  ViewExtensions.swift
//  MDThemeManager
//
//  Created by Andrés Alexis Rivas Solorzano on 06-03-18.
//

import Foundation

extension UIView{
    
    func addShadow(width: Double, height: Double) {
        
        self.layer.masksToBounds = false
        self.layer.shadowOffset = CGSize.init(width: width, height: height)
        self.layer.shadowRadius = 5
        self.layer.shadowOpacity = 0.4
    }
    
}
