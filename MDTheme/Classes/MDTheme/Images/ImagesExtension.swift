//
//  ImagesExtension.swift
//  MDThemes
//
//  Created by Andrés Alexis Rivas Solorzano on 06-02-18.
//  Copyright © 2018 Andrés Alexis Rivas Solorzano. All rights reserved.
//

import Foundation
import UIKit

extension UIImage{
    
    @nonobjc private static var resourceBundle: Bundle{
        return Bundle.init(for: SearchIconImageView.self)
    }
    
    @nonobjc public class func getInternalDBLogoIcon() -> String?{
        //        let image = MDRealmDataBase.realmData.objects(MDImageObject.self).filter("name == \"" + UIImage.logoName + "\"").first
        //        return UIImage.init(data: image!.dataImage!)
        return self.getInternalDBImg(name: ThemeImagesId.logoName)
    }
    
    @nonobjc public class func getInternalDBOfferIcon() -> String?{
        return UIImage.getInternalDBImg(name: ThemeImagesId.offerName)
    }
    
    @nonobjc public class func getInternalDBLoginBgImg() -> String?{
        return UIImage.getInternalDBImg(name: ThemeImagesId.backgroundName)
    }
    
    @nonobjc public class func getInternalDBImg(name: String) -> String?{
        guard let image = MDRealmDataBase.realmData.objects(MDImageObject.self).filter("name == \"" + name + "\"").last else{
            return nil
        }
        
        return image.remote_url_value
    }
    
    
    @nonobjc public class var navBarSearchIcon: UIImage?{
        return UIImage.init(named: ThemeImagesId.searchIconName, in: self.resourceBundle, compatibleWith: nil)?.withRenderingMode(.alwaysTemplate)

    }
    
    @nonobjc public class var navBarHomeIcon: UIImage?{
        return UIImage.init(named: ThemeImagesId.homeIconName, in: self.resourceBundle, compatibleWith: nil)?.withRenderingMode(.alwaysTemplate)
        
    }
    
    @nonobjc public class var navBarShopIcon: UIImage?{
        return UIImage.init(named: ThemeImagesId.shopIconName, in: self.resourceBundle, compatibleWith: nil)?.withRenderingMode(.alwaysTemplate)
    }
    
    @nonobjc public class var navBarMoreIcon: UIImage?{
        return UIImage.init(named: ThemeImagesId.moreIconName, in: self.resourceBundle, compatibleWith: nil)?.withRenderingMode(.alwaysTemplate)
    }
    
    @nonobjc public class var navBarScanIcon: UIImage?{
        return UIImage.init(named: ThemeImagesId.scanIconName, in: self.resourceBundle, compatibleWith: nil)?.withRenderingMode(.alwaysTemplate)
    }
    
    @nonobjc public class func scalableExternalImg (name: String, bundle: Bundle) -> UIImage?{
        return UIImage.init(named: name, in: bundle, compatibleWith: nil)?.withRenderingMode(.alwaysTemplate)
    }
    
   
    
}
