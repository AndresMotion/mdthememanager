//
//  ImageViewExtensions.swift
//  MDThemeManager
//
//  Created by Andrés Alexis Rivas Solorzano on 12-03-18.
//

import Foundation

extension UIImageView{

    @nonobjc public func borderAndColorImage(colorBorder: UIColor, radius: Float){
        self.layer.borderWidth = 2.0
        self.layer.borderColor = colorBorder.cgColor
        self.layer.cornerRadius = self.frame.height / 2
        self.layer.masksToBounds = true
    }
}
