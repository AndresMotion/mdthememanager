//
//  LogoImageView.swift
//  MDThemes
//
//  Created by Andrés Alexis Rivas Solorzano on 06-02-18.
//  Copyright © 2018 Andrés Alexis Rivas Solorzano. All rights reserved.
//

import Foundation
import UIKit

public class LogoImageView: UIImageView {
    
}

public class IconImageView: UIImageView {
    
}

public class LoginBackgroundView: UIImageView{
    
}

public class SearchIconImageView: UIImageView{
    
}

public class HomeIconImageView: UIImageView{
    
}

public class ShopIconImageView: UIImageView{
    
}

public class ScanIconImageView: UIImageView{
    
}

public class MoreIconImageView: UIImageView{
    
}

public class NavBarIconImageView: UIImageView{
    
    public var iconId: String! = ""
    
}

extension Style where UIElement: UIImageView{
    
    static public func GenerateNavBarIcon(iconImage: UIImage?) -> Style{
        return Style{ imageView in
            imageView.image = iconImage
            imageView.tintColor = Theme.current.navBarContrastColor
            imageView.contentMode = .center
        }
    }
    
    
}
