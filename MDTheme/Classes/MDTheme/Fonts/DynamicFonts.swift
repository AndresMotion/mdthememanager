//
//  DynamicFonts.swift
//  MDThemeManager
//
//  Created by Andrés Alexis Rivas Solorzano on 22-02-18.
//

import Foundation
import UIKit

protocol FontLoadedProtocol {
    func changeLoadStatus(newStatus: Bool)
}


//public class MDDynamicFont{
//    
//    
//    
//    public static func loadFont(font: MDFontObject, completion: @escaping ()-> ()){
//        
//        let documentsUrl =  FileManager.default.urls(for: .documentDirectory, in: .userDomainMask).first! as NSURL
//        let destinationUrl = documentsUrl.appendingPathComponent(font.file_name)
//        
//        self.loadGetRegisteredFont(localFile: destinationUrl!.absoluteString, hasError:  { (error: Bool) in
//            if error {
//                self.setNewFont(font: font, completion: { _ in
//                    print("Completed with inside error")
//                    completion()
//                })
//            } else{
//                print("Completed with no error")
//                completion()
//            }
//        })
//    }
//    
//    public static func setNewFont(font: MDFontObject, completion: @escaping (Bool) -> ()){
//        
//        guard let urlFont = URL.init(string: font.remote_url_value) else{
//            print("Error url no existe")
//            return
//        }
//        
//        let documentsUrl =  FileManager.default.urls(for: .documentDirectory, in: .userDomainMask).first! as NSURL
//        let destinationUrl = documentsUrl.appendingPathComponent(font.file_name)
//        
//        Downloader.load(url: urlFont, to: destinationUrl!, completion: {
//            print("descargo")
//            self.loadGetRegisteredFont(localFile: destinationUrl!.absoluteString, hasError: { error in
//            
//                completion(error)
//            })
//        })
//        
//    }
//    
//    public static func loadGetRegisteredFont(localFile: String, hasError: ((Bool) -> ())?){
//        
//        guard let localFileUrl = URL.init(string: localFile) else{
//            hasError?(true)
//            print("Error direccion no existe")
//            return
//        }
//        
////        var uiFont : UIFont?
//        guard let fontData = NSData(contentsOf: localFileUrl) else{
//            hasError?(true)
//            return
//        }
//        guard let dataProvider = CGDataProvider(data: fontData) else{
//            hasError?(true)
//            return
//        }
//        guard let cgFont = CGFont.init(dataProvider) else{
//            hasError?(true)
//            return
//        }
//        var errorFont: Unmanaged<CFError>?
//        if !CTFontManagerRegisterGraphicsFont(cgFont, &errorFont){
////            print("Error loading Font!", errorFont)
//            hasError?(true)
//        } else {
////            let fontName = cgFont.postScriptName
////            uiFont = UIFont(name: fontName! as String, size: 30)
//            
//            hasError?(false)
//            
//        }
//        
//    }
//}
//
//class Downloader {
//    class func load(url: URL, to localUrl: URL, completion: @escaping () -> ()) {
//        let sessionConfig = URLSessionConfiguration.default
//        let session = URLSession(configuration: sessionConfig)
////        let request = try! URLRequest(url: url, method: .get)
//        
//        let request = URLRequest(url: url, cachePolicy: .reloadIgnoringCacheData)
//        
//        let task = session.downloadTask(with: request) { (tempLocalUrl, response, error) in
//            if let tempLocalUrl = tempLocalUrl, error == nil {
//              
//                if let statusCode = (response as? HTTPURLResponse)?.statusCode {
//                    print("Success: \(statusCode)")
//                }
//                
//                do {
//                    try FileManager.default.copyItem(at: tempLocalUrl, to: localUrl)
//                    completion()
//                } catch (let writeError) {
//                    print("error writing file \(localUrl) : \(writeError)")
//                    completion()
//                }
//                
//            } else {
//                print("Failure: %@ \(String(describing: error?.localizedDescription))")
//            }
//        }
//        task.resume()
//    }
//}
