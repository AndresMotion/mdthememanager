//
//  FontsTheme.swift
//  MDThemes
//
//  Created by Andrés Alexis Rivas Solorzano on 06-02-18.
//  Copyright © 2018 Andrés Alexis Rivas Solorzano. All rights reserved.
//

import Foundation
import UIKit



extension UIFont{
    
    @nonobjc public class var regularFontName: String{

        guard let fontData = MDRealmDataBase.realmData.objects(MDFontObject.self).filter("name == \"" + ThemeFontsId.regularFontName + "\"").last else {
            return UIFont.systemFont(ofSize: 16).familyName
        }
        return fontData.font_family_name
    }
    
    @nonobjc public class var boldFontName: String{

        guard let fontData = MDRealmDataBase.realmData.objects(MDFontObject.self).filter("name == \"" + ThemeFontsId.boldFontName + "\"").last else{
            return UIFont.boldSystemFont(ofSize: 16).familyName
        }
        return fontData.font_family_name
    }
    
    
    @nonobjc public class var LoginTitleFont: UIFont {
        
        let newTitleFont = UIFont(name: boldFontName, size: CGFloatValues.LogintTitleFontSize)
        return newTitleFont ?? UIFont.systemFont(ofSize: CGFloatValues.LogintTitleFontSize)
        
    }
    
    @nonobjc public class var LoginParamNameFont: UIFont {
        return UIFont(name: boldFontName, size: 16.0)!
    }
    
    @nonobjc public class var LoginButtonFont: UIFont {
        return UIFont(name: boldFontName, size: 27.0)!
    }
    
    @nonobjc public class var CategoryTitleFont: UIFont {
        return UIFont(name: regularFontName, size: 30)!
    }
    
    @nonobjc public class var CategorySubtitleFont: UIFont {
        return UIFont(name: regularFontName, size: 33)!
    }
    
    @nonobjc public class var CategoryNormalNameFont: UIFont {
        return UIFont(name: boldFontName, size: 25)!
    }

    @nonobjc public class var CategoryMediumNameFont: UIFont {
        return UIFont(name: boldFontName, size: 35)!
    }
    
    @nonobjc public class var CategoryHeavyNameFont: UIFont {
        return UIFont(name: boldFontName, size: 45)!
    }
    
    @nonobjc public class var SubCategoryNameFont: UIFont {
        return UIFont(name: regularFontName, size: 16)!
    }
    
    @nonobjc public class var ProductGridSubtitleFont: UIFont {
        return UIFont(name: regularFontName, size: 27)!
    }
    
    @nonobjc public class var ProductGridNameFont: UIFont {
        return UIFont(name: boldFontName, size: 13)!
    }
    
    @nonobjc public class var ProductGridDescriptionFont: UIFont {
        return UIFont(name: regularFontName, size: 14)!
    }
    
    @nonobjc public class var ProductGridBestPriceFont: UIFont {
        return UIFont(name: boldFontName, size: 30)!
    }
    
    @nonobjc public class var ProductGridDiscountFont: UIFont {
        return UIFont(name: regularFontName, size: 12)!
    }
    
    @nonobjc public class var ProductGridOtherPriceFont: UIFont {
        return UIFont(name: regularFontName, size: 12)!
    }
   
    @nonobjc public class var ProductGridCompareFont: UIFont {
        return UIFont(name: boldFontName, size: 21)!
    }
    
    @nonobjc public class var ProductGridCancelCompareFont: UIFont {
        return UIFont(name: regularFontName, size: 15)!
    }
    
    @nonobjc public class var ProductComparePriceFont: UIFont {
        return UIFont(name: boldFontName, size: 26)!
    }
    
    @nonobjc public class var ProductCompareFeatTitleFont: UIFont {
        return UIFont(name: boldFontName, size: 15)!
    }
    
    @nonobjc public class var ProductCompareFeatValueFont: UIFont {
        return UIFont(name: regularFontName, size: 19)!
    }
    
    @nonobjc public class var ProductDetailBrandFont: UIFont {
        return UIFont(name: boldFontName, size: 24)!
    }
    
    @nonobjc public class var ProductDetailNameFont: UIFont {
        return UIFont(name: regularFontName, size: 19)!
    }
    
    @nonobjc public class var ProductDetailInfoFont: UIFont {
        return UIFont(name: boldFontName, size: 14)!
    }
    
    @nonobjc public class var ProductDetailSkuFont: UIFont {
        return UIFont(name: boldFontName, size: 12)!
    }
    
    @nonobjc public class var ProductDetailFeatValueFont: UIFont {
        return UIFont(name: regularFontName, size: 14)!
    }
    
    @nonobjc public class var ProductDetailSimilarBrandFont: UIFont {
        return UIFont(name: boldFontName, size: 10)!
    }
    
    @nonobjc public class var ProductDetailSimilarBestPriceFont: UIFont {
        return UIFont(name: boldFontName, size: 21)!
    }
    
    @nonobjc public class var ProductDetailSimilarOtherPriceFont: UIFont {
        return UIFont(name: regularFontName, size: 10)!
    }
    
    @nonobjc public class var ShoppingCartProductCantFont: UIFont {
        return UIFont(name: regularFontName, size: 21)!
    }
    
    @nonobjc public class var ShoppingCartTotalTitleFont: UIFont {
        return UIFont(name: regularFontName, size: 15)!
    }
    
    @nonobjc public class var ShoppingCartTotalValueFont: UIFont {
        return UIFont(name: boldFontName, size: 28)!
    }
    
    @nonobjc public class var ShoppingCartProductBrandFont: UIFont {
        return UIFont(name: boldFontName, size: 13)!
    }
    
    @nonobjc public class var ShoppingCartProductNameFont: UIFont {
        return UIFont(name: regularFontName, size: 13)!
    }
    
    @nonobjc public class var ShoppingCartProductSkuFont: UIFont {
        return UIFont(name: regularFontName, size: 10)!
    }
    
    @nonobjc public class var ShoppingCartProductBestPrice: UIFont {
        return UIFont(name: boldFontName, size: 22)!
    }
    
    @nonobjc public class var ShoppingCartProductOtherPrice: UIFont {
        return UIFont(name: regularFontName, size: 11)!
    }
    
    @nonobjc public class var ShoppingCartProductCant: UIFont {
        return UIFont(name: regularFontName, size: 10)!
    }
    
    @nonobjc public class var ShoppingCartProdCantValue: UIFont {
        return UIFont(name: regularFontName, size: 22)!
    }
    
    @nonobjc public class var boldTextFont: UIFont {
        return UIFont(name: boldFontName, size: 20.0)!
    }
    
    @nonobjc public class var ProductDetailBestPriceValueFont: UIFont {
        return UIFont(name: boldFontName, size: 29.0)!
    }
    
    @nonobjc public class var ProductDetailBestPriceTitleFont: UIFont {
        return UIFont(name: boldFontName, size: 17.0)!
    }
    
    @nonobjc public class var ProductDetailOtherPriceFont: UIFont {
        return UIFont(name: regularFontName, size: 19.0)!
    }
    
    @nonobjc public class var ProductDetailOtherPriceTitleFont: UIFont {
        return UIFont(name: regularFontName, size: 16.0)!
    }
    
    @nonobjc public class var loginTextFieldFont: UIFont {
        return UIFont(name: regularFontName, size: 15.0)!
    }
    
    @nonobjc public class var recoverPasswordtFont: UIFont {
        return UIFont(name: regularFontName, size: 13.0)!
    }
    
    @nonobjc public class var loginButtonFont: UIFont {
        return UIFont(name: "AzoSans-Medium", size: 18.0)!
    }
    
    
}

