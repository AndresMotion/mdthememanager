//
//  ColorObject.swift
//  MDThemeManager
//
//  Created by Andrés Alexis Rivas Solorzano on 21-02-18.
//

import Foundation
import RealmSwift
//public final class TaskList: Object {
//    @objc dynamic var text = ""
//    @objc dynamic var id = ""
//    let items = List<Task>()
//
//    override public static func primaryKey() -> String? {
//        return "id"
//    }
//}
//
//public final class Task: Object {
//    @objc dynamic var text = ""
//    @objc dynamic var completed = false
//}

public final class MDColorObject: Object{
    
    @objc public dynamic var name = ""
    @objc public dynamic var hex_value = ""
    
}
