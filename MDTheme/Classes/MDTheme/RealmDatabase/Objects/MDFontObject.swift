//
//  MDFontObject.swift
//  MDThemeManager
//
//  Created by Andrés Alexis Rivas Solorzano on 22-02-18.
//

import Foundation
import RealmSwift

public final class MDFontObject: Object{
    
    @objc public dynamic var name = ""
    @objc public dynamic var type = ""
    @objc public dynamic var loaded = false
    @objc public dynamic var file_name = ""
    @objc public dynamic var font_family_name = ""
    @objc public dynamic var remote_url_value = ""
    @objc public dynamic var local_url_value = ""
    
}
