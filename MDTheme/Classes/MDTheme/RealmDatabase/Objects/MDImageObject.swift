//
//  ImageObject.swift
//  MDThemeManager
//
//  Created by Andrés Alexis Rivas Solorzano on 21-02-18.
//

import Foundation
import RealmSwift

public final class MDImageObject: Object{
    
    @objc public dynamic var name = ""
    @objc public dynamic var remote_url_value = ""
    @objc public dynamic var dataImage: Data?
    
    
}
