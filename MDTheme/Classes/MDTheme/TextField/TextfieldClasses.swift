//
//  File.swift
//  MDThemes
//
//  Created by Andrés Alexis Rivas Solorzano on 08-02-18.
//  Copyright © 2018 Andrés Alexis Rivas Solorzano. All rights reserved.
//

import Foundation
import UIKit

class LoginTextField: UITextField {
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        self.layer.borderWidth = 1
        self.layer.borderColor = Theme.current.mainColor.cgColor
    }
    
}

class UserTextField: LoginTextField {

}

class PasswordTextField: LoginTextField {

}
