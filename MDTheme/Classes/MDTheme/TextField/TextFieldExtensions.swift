//
//  TextFieldExtensions.swift
//  MDThemes
//
//  Created by Andrés Alexis Rivas Solorzano on 08-02-18.
//  Copyright © 2018 Andrés Alexis Rivas Solorzano. All rights reserved.
//

import Foundation
import UIKit

extension UITextField{
    
    func setSimpleBorderColor(color: CGColor, borderWidth: CGFloat? = 1.0){
        self.layer.borderWidth = 1.0
        self.layer.borderColor = color
    }
    
    
}
