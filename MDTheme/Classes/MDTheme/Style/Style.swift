//
//  Style.swift
//  MDThemes
//
//  Created by Andrés Alexis Rivas Solorzano on 19-02-18.
//  Copyright © 2018 Andrés Alexis Rivas Solorzano. All rights reserved.
//

import Foundation
import UIKit

public struct Style<UIElement: Stylable>{
    public let applyTo: (UIElement) -> Void
}

public protocol Stylable: class{
    func apply(styles: Style<UIView>... )
}


public extension Stylable where Self: UIView{
    public func apply(styles: Style<Self>... ){
        styles.forEach{ $0.applyTo(self) }
    }
}
extension UIView: Stylable {
    public func apply(styles: Style<UIView>...) {
        styles.forEach{ $0.applyTo(self) }
    }
}





