//
//  DynamicTheme.swift
//  MDThemes
//
//  Created by Andrés Alexis Rivas Solorzano on 07-02-18.
//  Copyright © 2018 Andrés Alexis Rivas Solorzano. All rights reserved.
//

import Foundation
import RealmSwift

public struct MDRealmDataBase {
    
    public static let shared = MDRealmDataBase()
    
    public static var  realmData: Realm{
        return try! Realm()
    }
    
    public func setColor(name: String, hexValue: String){
        
        let newColor = MDColorObject()
        newColor.name = name
        newColor.hex_value = hexValue
        
        try! MDRealmDataBase.realmData.write {
            MDRealmDataBase.realmData.add(newColor)
        }
    }
    
    public func saveNewFont(newFont: MDFontObject){
        try! MDRealmDataBase.realmData.write {
            MDRealmDataBase.realmData.add(newFont)
        }
    }
    
    public func setImage(imageObject: MDImageObject){
        
        let url = URL.init(string: imageObject.remote_url_value)!
        print("url image", url)
        if let imgData = NSData.init(contentsOf: url){
            let newImage = MDImageObject()
            newImage.name = imageObject.name
            newImage.remote_url_value = imageObject.remote_url_value
            newImage.dataImage = imgData as Data
            try! MDRealmDataBase.realmData.write {
                MDRealmDataBase.realmData.add(newImage)
            }
        } else{
            print("failed download img")
        }
        print("completed")
    }
    
}

