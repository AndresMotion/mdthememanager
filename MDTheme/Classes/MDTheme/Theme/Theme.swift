//
//  Theme.swift
//  MDThemes
//
//  Created by Andrés Alexis Rivas Solorzano on 30-01-18.
//  Copyright © 2018 Andrés Alexis Rivas Solorzano. All rights reserved.
//

import Foundation
import UIKit
import RealmSwift

public enum Theme: Int {
    case `default`, custom
    
    private enum Keys{
        static let selectedTheme = "SelectedTheme"
    }
    
    public static var current: Theme{
        
        let storedTheme = UserDefaults.standard.integer(forKey: Keys.selectedTheme)
        return Theme.init(rawValue: storedTheme) ?? .default
    
    }
    
    public var themeName : String{
        
        switch self {
        case .default:
            return "default"
        case .custom:
            return "custom"
        }
    }
    
    public var mainColor: UIColor{
        
        switch self {
        case .default:
            return UIColor.init(red: 85.0/255.0, green: 190.0/255.0, blue: 98.0/255.0, alpha: 1.0)
        case .custom:
            return UIColor.testGetMainColor()
        }
    }
    
    public var secondaryColor: UIColor{
        switch self {
        case .default:
            return .green
       
        case .custom:
//            ACTUALIZAR A SECONDARY COLOR DINAMICO
            return UIColor.generateInternalDBColor(colorName: ThemeColorsId.secondaryColorName)
        }
    }
    
    public var accentColor: UIColor{
        switch self {
        case .default:
            return .red
        case .custom:
            return UIColor.generateInternalDBColor(colorName: ThemeColorsId.accentColorName)
        }
    }
    
    public var navBarBackGroundColor: UIColor{
        switch self {
        case .default:
            return UIColor.brown
        default:
            return UIColor.generateInternalDBColor(colorName: ThemeColorsId.navBarBackgroumdColorName)
        }
    }
    
    public var navBarContrastColor: UIColor{
        switch self {
        case .default:
            return UIColor.hexStringToUIColor(hex: ThemeDefaultId.defaultWhiteColorHex)
        case .custom:
            return UIColor.generateInternalDBColor(colorName: ThemeColorsId.navBarContrastColorName)
        }
    }
    
    
    
    public func loadTheme(completion: @escaping () -> ()){
//        let semaphore = DispatchSemaphore(value: 1)
//
//        let fontList = MDRealmDataBase.realmData.objects(MDFontObject.self)
//
//        for font in fontList{
//            semaphore.wait()
//            MDDynamicFont.loadFont(font: font, completion: {
//                semaphore.signal()
//            })
//        }
        
        completion()
    }
    
    @available(iOS 9.0, *)
    public func newTheme(paletaColores: Array<MDColorObject>, assetsImagenes: Array<MDImageObject>, fontAssets: Array<MDFontObject>, completion: @escaping ()->()){
        
        for color in paletaColores{
            MDRealmDataBase.shared.setColor(name: color.name, hexValue: color.hex_value)
        }
        
        for image in assetsImagenes{
            MDRealmDataBase.shared.setImage(imageObject: image)
        }
        
//        let semaphore = DispatchSemaphore(value: 1)
        
//        let myGroup = DispatchGroup()
        for font in fontAssets{
            MDRealmDataBase.shared.saveNewFont(newFont: font)
//            myGroup.enter()
//            semaphore.wait()
//            MDDynamicFont.loadFont(font: font, completion: {
//                semaphore.signal()
////                myGroup.leave()
//            })
//            print("iteration end")
        }
        
//        myGroup.notify(queue: .main) {
//            self.apply()
            completion()
//        }
    }
    
    
    func setupNavigationBarAppareance(){
        UINavigationBar.appearance().barTintColor = self.navBarBackGroundColor
        UINavigationBar.appearance().tintColor = self.navBarContrastColor
        
//        UINavigationBar.appearance().setBackgroundImage(navigationBackgroundImage, for: .default)
//        UINavigationBar.appearance().backIndicatorImage = UIImage.init(named: "backArrow")
//        UINavigationBar.appearance().backIndicatorTransitionMaskImage = UIImage.init(named: "backArrowMaskFixed")
    }
    func setupButtonAppareance(){

        LoginButton.appearance().backgroundColor = self.mainColor
        LoginButton.appearance().setAttributedTextAndColor("Entrar",.white, 18.0, UIFont.regularFontName)
        
    }
    
    @available(iOS 9.0, *)
    func setupLabelAppareance(){
        
//        UILabel.appearance(whenContainedInInstancesOf: [UIButton.self]).font = UIFont.LoginButtonFont
        TitleLabel.appearance().font = .boldTextFont
        SubTitleLabel.appearance().font = .LoginButtonFont
        BestPriceLabel.appearance().font = .ProductDetailBestPriceValueFont
        OtherPriceLabel.appearance().font = .ProductDetailOtherPriceFont
        BestPriceTitleLabel.appearance().font = .ProductDetailBestPriceTitleFont
        OtherPriceTitleLabel.appearance().font = .ProductDetailOtherPriceTitleFont
        
        BestPriceTitleLabel.appearance().textColor = UIColor.mdBestPriceColor
        BestPriceLabel.appearance().textColor = UIColor.testGetMainColor()
        OtherPriceTitleLabel.appearance().textColor = UIColor.mdOtherPriceColor
        OtherPriceLabel.appearance().textColor = UIColor.mdOtherPriceColor
        
        
    }
    
    func setupImageView(){
        
//        LogoImageView.appearance().image = UIImage.getInternalDBLogoIcon()
//        IconImageView.appearance().image = UIImage.getInternalDBOfferIcon()
//        LoginBackgroundView.appearance().image = UIImage.getInternalDBLoginBgImg()
//        
        SearchIconImageView.appearance().apply(styles: Style.GenerateNavBarIcon(iconImage: UIImage.navBarSearchIcon))
        MoreIconImageView.appearance().apply(styles: Style.GenerateNavBarIcon(iconImage: UIImage.navBarMoreIcon))
        ShopIconImageView.appearance().apply(styles: Style.GenerateNavBarIcon(iconImage: UIImage.navBarShopIcon))
        ScanIconImageView.appearance().apply(styles: Style.GenerateNavBarIcon(iconImage: UIImage.navBarScanIcon))
        HomeIconImageView.appearance().apply(styles: Style.GenerateNavBarIcon(iconImage: UIImage.navBarHomeIcon))

        
    }
    
    func setupTextFields(){
        
//        UserTextField.appearance().placeholder = "Usuario"
//        PasswordTextField.appearance().placeholder = "Contraseña"
//        LoginTextField.appearance().font = UIFont.loginTextFieldFont
//        LoginTextField.appearance().textColor = UIColor.pgWarmGrey
//        LoginTextField.appearance().leftViewMode = .always
//        UserTextField.appearance().layer.borderWidth = 10.0
//        UserTextField.appearance().layer.borderColor = UIColor.red.cgColor
//        UserTextField.appearance().layer.masksToBounds = true
        
    }
    
    
    @available(iOS 9.0, *)
    public func apply(){
        
        UserDefaults.standard.set(rawValue, forKey: Keys.selectedTheme)
        print(rawValue)
        UserDefaults.standard.synchronize()
        
        UIApplication.shared.delegate?.window??.tintColor = mainColor
        
        
        setupNavigationBarAppareance()
        setupButtonAppareance()
        setupLabelAppareance()
        setupImageView()
//        setupTextFields()
        
//        // TABBAR
//        UITabBar.appearance().backgroundColor = self.mainColor
//        UITabBar.appearance().backgroundImage = tabBarBackgroundImage
//
//        let tabIndicator = UIImage.init(named: "tabBarSelectionIndicator")?.withRenderingMode(.alwaysTemplate)
//        let tabResizableIndicator = tabIndicator?.resizableImage(withCapInsets: UIEdgeInsets.init(top: 0, left: 0, bottom: 0, right: 2.0))
//        UITabBar.appearance().selectionIndicatorImage = tabResizableIndicator
//
//        //UISegmentedControl
//        let controlBackground = UIImage.init(named: "controlBackground")?
//            .withRenderingMode(.alwaysTemplate)
//            .resizableImage(withCapInsets: UIEdgeInsets.init(top: 3, left: 3, bottom: 3, right: 3))
//
//        let selectedControlBackground = UIImage.init(named: "controlSelectedBackground")?
//            .withRenderingMode(.alwaysTemplate)
//            .resizableImage(withCapInsets: UIEdgeInsets.init(top: 3, left: 3, bottom: 3, right: 3))
//
//        UISegmentedControl.appearance()
//            .setBackgroundImage(controlBackground, for: .normal, barMetrics: .default)
//
//        UISegmentedControl.appearance()
//            .setBackgroundImage(selectedControlBackground, for: .selected, barMetrics: .default)
//
//        //UIStepper
//        UIStepper.appearance().setBackgroundImage(controlBackground, for: .normal)
//        UIStepper.appearance().setBackgroundImage(controlBackground, for: .disabled)
//        UIStepper.appearance().setBackgroundImage(controlBackground, for: .highlighted)
//
//        UIStepper.appearance().setDecrementImage(UIImage.init(named: "fewerPaws"), for: .normal)
//        UIStepper.appearance().setIncrementImage(UIImage.init(named: "morePaws"), for: .normal)
//
//        //    UISlider
//        UISlider.appearance().setMaximumTrackImage(UIImage.init(named: "maximumTrack")?
//            .resizableImage(withCapInsets: UIEdgeInsets.init(top: 0, left: 0, bottom: 0, right: 6.0)), for: .normal)
//        UISlider.appearance().setMinimumTrackImage(UIImage.init(named: "minimumTrack")?
//            .withRenderingMode(.alwaysTemplate)
//            .resizableImage(withCapInsets: UIEdgeInsets.init(top: 0, left: 6.0, bottom: 0, right: 0.0)), for: .normal)
//
//        UISlider.appearance().setThumbImage(UIImage.init(named: "sliderThumb"), for: .normal)
//
//        //    UISwitch
//        UISwitch.appearance().onTintColor = mainColor.withAlphaComponent(0.4)
//        UISwitch.appearance().thumbTintColor = mainColor
//
//        //    UITableViewCell
//        UITableViewCell.appearance().backgroundColor = backgroundCellColor
//        UILabel.appearance(whenContainedInInstancesOf: [UITableViewCell.self]).textColor = textCellColor
//
        
        if #available(iOS 10.3, *) {
//            self.setIcon(name: self.themeName)
        } else {
            // Fallback on earlier versions
        }
        
        
    }
    
    @available(iOS 10.3, *)
    func setCustomAppIcon(name: String?){
        guard let iconName = name else {
            print("no existe icono")
            return
        }
        UIApplication.shared.setAlternateIconName(iconName){ error in
            if let error = error {
                print(error.localizedDescription)
            } else {
                print("Done!")
            }
        }
    }
    
    var backgroundCellColor: UIColor{
        switch self {
        case .default:
            return .white
        case .custom:
            return UIColor.init(white: 0.4, alpha: 1.0)
        }
    }
    var textCellColor: UIColor{
        switch self {
        case .default:
            return .black
        case .custom:
            return .white
        }
    }
    var barStyle: UIBarStyle{
        switch self {
        case .default:
            return .default
        case .custom:
            return .black
        }
    }
    
    
}


