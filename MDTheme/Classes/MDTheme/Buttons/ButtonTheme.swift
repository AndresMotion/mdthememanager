//
//  ButtonTheme.swift
//  MDThemes
//
//  Created by Andrés Alexis Rivas Solorzano on 06-02-18.
//  Copyright © 2018 Andrés Alexis Rivas Solorzano. All rights reserved.
//

import Foundation
import UIKit

extension UIButton{
    
    public func setAttributedTextAndColor(_ text: String, _ color: UIColor, _ size: CGFloat, _ font: String? = UIFont.regularFontName, _ state: UIControl.State = .normal, _ justified: Bool = false){
        
        var attrString = NSAttributedString()
        let newFont = UIFont.init(name: font!, size: size)!
        if !justified{
            attrString = NSAttributedString(string: text, attributes: [NSAttributedString.Key.font: newFont, NSAttributedString.Key.foregroundColor : color])
        }else{
            let style = NSMutableParagraphStyle()
            style.alignment = NSTextAlignment.center
             attrString = NSAttributedString(string: text, attributes: [NSAttributedString.Key.font: newFont, NSAttributedString.Key.foregroundColor : color, NSAttributedString.Key.paragraphStyle: style])
        }
        self.setAttributedTitle(attrString, for: state)
        
    }
    
    public func borderButton(color: CGColor? = nil , backgroundColor: UIColor? = .white, borderRadius:CGFloat = 2.0){
        let colorBorde = color == nil ? self.layer.borderColor : color
        self.layer.borderWidth = 1.0
        self.layer.borderColor = colorBorde
        self.layer.cornerRadius = self.frame.height/borderRadius
        self.backgroundColor = backgroundColor
    }
    
    
}
