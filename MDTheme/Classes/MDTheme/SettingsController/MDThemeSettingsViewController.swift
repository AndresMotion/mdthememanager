//
//  MDThemeSettingsViewController.swift
//  MDThemes
//
//  Created by Andrés Alexis Rivas Solorzano on 30-01-18.
//  Copyright © 2018 Andrés Alexis Rivas Solorzano. All rights reserved.
//

import UIKit

class MDThemeSettingsViewController: UIViewController {

    @IBOutlet weak var themeSelector: UISegmentedControl!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        themeSelector.selectedSegmentIndex = Theme.current.rawValue == 2 ? 1 : 0
       
//          self.navigationController!.navigationBar.barTintColor = Theme.current.mainColor
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

   
    

}

private extension MDThemeSettingsViewController {
    
    @objc func dismissAnimated() {
        dismiss(animated: true)
    }
    
    @available(iOS 9.0, *)
    @IBAction func applyTheme(_ sender: UIButton) {
        if let selectedTheme = Theme(rawValue: themeSelector.selectedSegmentIndex + 1){
            selectedTheme.apply()
        }
        self.dismiss(animated: true, completion: nil)
    }
}
