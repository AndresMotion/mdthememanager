//
//  CGFloatExtensions.swift
//  MDThemes
//
//  Created by Andrés Alexis Rivas Solorzano on 09-02-18.
//  Copyright © 2018 Andrés Alexis Rivas Solorzano. All rights reserved.
//

import Foundation
import UIKit


protocol FontConstant {
    
}

struct CGIpadFonts: FontConstant{
    static let LoginTitleFont: CGFloat = 33.0
    
}

struct CGIphoneFonts: FontConstant{
    static let LogintTitleFont: CGFloat = 30.0
}

enum DeviceFonts: Int{
    
    case ipad, iphone
    
    public static var currentDevice: DeviceFonts{
        
//        let storedTheme = UserDefaults.standard.integer(forKey: Keys.selectedTheme)
        let current = MDevice.currentDevice.userInterfaceIdiom == .pad ? self.ipad.rawValue : self.iphone.rawValue
        return DeviceFonts.init(rawValue: current) ?? .ipad
        
    }
    
    public var LoginTitleFont: CGFloat{
        switch  self {
        case .ipad:
            return CGIpadFonts.LoginTitleFont
        case .iphone:
            return CGIphoneFonts.LogintTitleFont
        }
    }
    
}

enum MDevice {

    static public var currentDevice: UIDevice{
        return UIDevice.current
    }

    static public var fontsSize: DeviceFonts {
        return self.currentDevice.userInterfaceIdiom == .pad ? DeviceFonts.ipad : DeviceFonts.iphone
    }

}

enum CGFloatValues{
    
    static private var currentDevice: UIDevice{
        return UIDevice.current
    }
    
    
    static public var LogintTitleFontSize: CGFloat{
//        return self.currentDevice.userInterfaceIdiom == UIUserInterfaceIdiom.pad ? CGIpadFonts.LoginTitleFont : CGIphoneFonts.LogintTitleFont
        return MDevice.fontsSize.LoginTitleFont
//        return DeviceFonts.currentDevice.LoginTitleFont
        
    }
    
}
