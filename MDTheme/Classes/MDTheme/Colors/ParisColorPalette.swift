//
//  ParisColorPalette.swift
//  MDThemes
//
//  Created by Andrés Alexis Rivas Solorzano on 30-01-18.
//  Copyright © 2018 Andrés Alexis Rivas Solorzano. All rights reserved.
//

import Foundation
import UIKit

extension UIColor {
    @nonobjc class var pgSoftBlue: UIColor {
        return UIColor(red: 91.0 / 255.0, green: 172.0 / 255.0, blue: 236.0 / 255.0, alpha: 1.0)
    }
    
    @nonobjc class var pgNeonPink: UIColor {
        return UIColor(red: 1.0, green: 0.0, blue: 155.0 / 255.0, alpha: 1.0)
    }
    
    @nonobjc class var pgWarmGrey: UIColor {
        return UIColor(white: 155.0 / 255.0, alpha: 1.0)
    }
    
    @nonobjc class var pgGreyishBrown: UIColor {
        return UIColor(white: 77.0 / 255.0, alpha: 1.0)
    }
    
    @nonobjc class var pgWarmGreyTwo: UIColor {
        return UIColor(white: 144.0 / 255.0, alpha: 1.0)
    }
    
    @nonobjc class var pgPinkishGrey: UIColor {
        return UIColor(white: 209.0 / 255.0, alpha: 1.0)
    }
    
    @nonobjc class var pgBlack12: UIColor {
        return UIColor(white: 0.0, alpha: 0.12)
    }
    
    @nonobjc class var pgAzure: UIColor {
        return UIColor(red: 0.0, green: 176.0 / 255.0, blue: 235.0 / 255.0, alpha: 1.0)
    }
    
    @nonobjc class var pgWhite: UIColor {
        return UIColor(white: 247.0 / 255.0, alpha: 1.0)
    }
    
    @nonobjc class var pgOrangeRed: UIColor {
        return UIColor(red: 1.0, green: 43.0 / 255.0, blue: 43.0 / 255.0, alpha: 1.0)
    }
    
    @nonobjc class var pgSunflowerYellow: UIColor {
        return UIColor(red: 239.0 / 255.0, green: 222.0 / 255.0, blue: 1.0 / 255.0, alpha: 1.0)
    }
    
    @nonobjc class var pgTangerine: UIColor {
        return UIColor(red: 1.0, green: 158.0 / 255.0, blue: 0.0, alpha: 1.0)
    }
    
    @nonobjc class var pgTealish: UIColor {
        return UIColor(red: 38.0 / 255.0, green: 194.0 / 255.0, blue: 172.0 / 255.0, alpha: 1.0)
    }
}
