//
//  SodimacColors.swift
//  MDThemes
//
//  Created by Andrés Alexis Rivas Solorzano on 30-01-18.
//  Copyright © 2018 Andrés Alexis Rivas Solorzano. All rights reserved.
//

import Foundation
import UIKit

extension UIColor {
    
    @nonobjc class var sgBluish: UIColor {
        return UIColor(red: 41.0 / 255.0, green: 129.0 / 255.0, blue: 196.0 / 255.0, alpha: 1.0)
    }
    
    @nonobjc class var sgGreyishBrown: UIColor {
        return UIColor(white: 74.0 / 255.0, alpha: 1.0)
    }
    
    @nonobjc class var sgWarmGrey: UIColor {
        return UIColor(white: 136.0 / 255.0, alpha: 1.0)
    }
    
    @nonobjc class var sgLipstick: UIColor {
        return UIColor(red: 213.0 / 255.0, green: 32.0 / 255.0, blue: 39.0 / 255.0, alpha: 1.0)
    }
    
    @nonobjc class var sgBlack: UIColor {
        return UIColor(white: 0.0, alpha: 1.0)
    }
    
    @nonobjc class var sgCerulean: UIColor {
        return UIColor(red: 0.0, green: 114.0 / 255.0, blue: 206.0 / 255.0, alpha: 1.0)
    }
    
    @nonobjc class var sgCherry: UIColor {
        return UIColor(red: 221.0 / 255.0, green: 0.0, blue: 33.0 / 255.0, alpha: 1.0)
    }
    
    @nonobjc class var sgBlack12: UIColor {
        return UIColor(white: 0.0, alpha: 0.12)
    }
    
    @nonobjc class var sgCoolGrey: UIColor {
        return UIColor(red: 153.0 / 255.0, green: 170.0 / 255.0, blue: 170.0 / 255.0, alpha: 1.0)
    }
    
    @nonobjc class var sgSunflowerYellow: UIColor {
        return UIColor(red: 251.0 / 255.0, green: 214.0 / 255.0, blue: 0.0, alpha: 1.0)
    }
    
    @nonobjc class var sgCoolGreyTwo: UIColor {
        return UIColor(red: 168.0 / 255.0, green: 187.0 / 255.0, blue: 187.0 / 255.0, alpha: 1.0)
    }
    
    @nonobjc class var sgPaleGrey: UIColor {
        return UIColor(red: 249.0 / 255.0, green: 252.0 / 255.0, blue: 252.0 / 255.0, alpha: 1.0)
    }
}
