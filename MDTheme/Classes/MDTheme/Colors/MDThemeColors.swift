//
//  MDThemeColors.swift
//  MDThemes
//
//  Created by Andrés Alexis Rivas Solorzano on 30-01-18.
//  Copyright © 2018 Andrés Alexis Rivas Solorzano. All rights reserved.
//

import Foundation
import UIKit

extension UIColor{
    
    
    @nonobjc public class func testGetMainColor() -> UIColor{
        guard let color = MDRealmDataBase.realmData.objects(MDColorObject.self).filter("name == \"" + ThemeColorsId.mainColorName + "\"").last else{
            return self.hexStringToUIColor(hex: "#ffffff" )
        }
        return self.hexStringToUIColor(hex: color.hex_value)
    }
    
    
    @nonobjc public class func generateInternalDBColor(colorName: String) -> UIColor{
        guard let color = MDRealmDataBase.realmData.objects(MDColorObject.self).filter("name == \"" + colorName + "\"").last else{
            return self.hexStringToUIColor(hex: ThemeDefaultId.defaultColorHexValue)
        }
        return self.hexStringToUIColor(hex: color.hex_value)
    }
    
    @nonobjc public class var mdThemeMainColor: UIColor{
        
        let mainColorValue = UserDefaults.standard.string(forKey: "MainColorHex") ?? "#ffffff"
        return UIColor.hexStringToUIColor(hex: mainColorValue)
        
    }
    
    @nonobjc public class var mdThemeSecondaryColor: UIColor{
        
        let secondaryColorValue = UserDefaults.standard.string(forKey: "SecondaryColorHex") ?? "#00000"
        return UIColor.hexStringToUIColor(hex: secondaryColorValue)
        
    }
    
    
    @nonobjc public static func setThemeMainColor(hexValue: String){
        
        UserDefaults.standard.set(hexValue, forKey: "MainColorHex")
        UserDefaults.standard.synchronize()
    
    }
    
    
    @nonobjc class var mdBestPriceColor: UIColor {
        return Theme.current == .default ? .black : .pgSoftBlue
    }
    
    @nonobjc class var mdOtherPriceColor: UIColor {
        return Theme.current == .default ? .sgCoolGrey : .black
    }
    
    @nonobjc public  class var mdWarmGrey: UIColor {
        return UIColor(white: 155.0 / 255.0, alpha: 1.0)
    }
    
    @nonobjc class var mdNavigationBackGroundColor: UIColor{
        return Theme.current == .default ? .white : .pgSoftBlue
    }
    
    @nonobjc class var mdNavigationItemsColor: UIColor{
        return Theme.current == .default ? .black : .white
    }
    
    @nonobjc class var mdNavigationButtonsBackgroundColor: UIColor{
        return .clear
    }
    
    @nonobjc class var mdNavigationButtonTextAndBorderColor: UIColor{
        return Theme.current == .default ? Theme.current.mainColor : .white
    }
    
    @nonobjc public class var mdGreyishBrown: UIColor {
        return UIColor(white: 77.0 / 255.0, alpha: 1.0)
    }
    
    public static func hexStringToUIColor (hex:String) -> UIColor {
        var cString:String = hex.trimmingCharacters(in: .whitespacesAndNewlines).uppercased()
        
        if (cString.hasPrefix("#")) {
            cString.remove(at: cString.startIndex)
        }
        
        if ((cString.count) != 6) {
            return UIColor.gray
        }
        
        var rgbValue:UInt32 = 0
        Scanner(string: cString).scanHexInt32(&rgbValue)
        
        return UIColor(
            red: CGFloat((rgbValue & 0xFF0000) >> 16) / 255.0,
            green: CGFloat((rgbValue & 0x00FF00) >> 8) / 255.0,
            blue: CGFloat(rgbValue & 0x0000FF) / 255.0,
            alpha: CGFloat(1.0)
        )
    }
    
}


