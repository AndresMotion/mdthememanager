//
//  MDStringConstants.swift
//  MDThemeManager
//
//  Created by Andrés Alexis Rivas Solorzano on 22-02-18.
//

import Foundation


public enum ThemeColorsId {
    
    public static let mainColorName = "MainColorHex"
    public static let accentColorName = "AccentColorHex"
    public static let secondaryColorName = "SecondaryColorHex"
    public static let navBarBackgroumdColorName = "NavBarBackgroundColorHex"
    public static let navBarContrastColorName = "NavBarContrastColorHex"

}

public enum ThemeDefaultId{
    public static let defaultColorHexValue = "#20B2AA"
    public static let defaultWhiteColorHex = "#FFFFFF"
}

public enum ThemeImagesId{
    
    public static let logoName = "MainLogoImg"
    public static let offerName = "MainOfferIcon"
    public static let backgroundName = "LoginBackgroundImg"
    
    public static let searchIconName = "NavBarSearchIcon"
    public static let moreIconName = "NavBarMoreIcon"
    public static let homeIconName = "NavBarHomeIcon"
    public static let scanIconName = "NavBarScanIcon"
    public static let shopIconName = "NavBarShopIcon"
    
}

public enum ThemeFontsId{
    
    public static let regularFontName = "RegularFont"
    public static let boldFontName = "BoldFont"
}
