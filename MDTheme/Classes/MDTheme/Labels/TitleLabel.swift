//
//  TitleLabel.swift
//  MDThemes
//
//  Created by Andrés Alexis Rivas Solorzano on 06-02-18.
//  Copyright © 2018 Andrés Alexis Rivas Solorzano. All rights reserved.
//

import Foundation
import UIKit

public class TitleLabel: UILabel{
}

public class LoginTitleLabel: UILabel{
    
}

public class BestPriceLabel: UILabel {
}

public class BestPriceTitleLabel: UILabel {
}


extension Style where UIElement: UILabel{
    
    public static func GenerateLabelStyle(font: UIFont, textColor: UIColor, backgroundColor: UIColor){
        
    }
    
    public static func GenericLabelStyle (font: UIFont, textColor: UIColor) -> Style {
        return Style { label in
            label.font = font
            label.textColor = textColor
        }
    }
    
    public static func GenerateLabelStyleFor(labelParam: UILabel) -> Style {
        return Style { label in
            
        }
    }
    
    public static var ProductDetailBrandStyle: Style{
        return Style.GenericLabelStyle(font: .ProductDetailBrandFont, textColor: UIColor.mdGreyishBrown)
    }
    
    public static var ProductDetailNameStyle: Style{
        return Style.GenericLabelStyle(font: .ProductDetailNameFont, textColor: UIColor.mdGreyishBrown)
    }
    
    public static var ProductDetailSkuStyle: Style{
        return Style.GenericLabelStyle(font: .ProductDetailSkuFont, textColor: UIColor.mdWarmGrey)
    }
    
    public static var ProductDetailInfoStyle: Style{
        return Style.GenericLabelStyle(font: .ProductDetailInfoFont, textColor: UIColor.mdWarmGrey)
    }
    
    public static var ProductDetailFeatTitleStyle: Style{
        return Style.GenericLabelStyle(font: .ProductCompareFeatTitleFont, textColor: Theme.current.accentColor)
    }
    
    public static var ProductDetailFeatValueStyle: Style{
        return Style.GenericLabelStyle(font: .ProductDetailFeatValueFont, textColor: .mdGreyishBrown)
    }
    
    public static var ProductDetailBestPriceTitleStyle : Style{
        return Style.GenericLabelStyle(font: .ProductDetailBestPriceTitleFont, textColor: Theme.current.accentColor)
    }
    
    public static var ProductDetailBestPriceValueStyle: Style{
        return Style.GenericLabelStyle(font: .ProductDetailBestPriceValueFont, textColor: Theme.current.accentColor)
    }
    
    public static var ProductDetailOtherPriceTitleStyle: Style{
        return Style.GenericLabelStyle(font: .ProductDetailOtherPriceTitleFont, textColor: UIColor.mdWarmGrey)
    }
    
    public static var ProductDetailOtherPriceValueStyle: Style{
        return Style.GenericLabelStyle(font: .ProductDetailOtherPriceTitleFont, textColor: UIColor.mdWarmGrey)
    }
    
    public static var ProductComparePriceStyle: Style{
        return Style.GenericLabelStyle(font: .ProductComparePriceFont, textColor: Theme.current.accentColor)
    }
    
    public static var ProductCompareFeatTitleStyle: Style{
        return Style.GenericLabelStyle(font: .ProductCompareFeatTitleFont, textColor: Theme.current.accentColor)
    }
    
    public  static var ProductCompareFeatValueStyle: Style{
        return Style.GenericLabelStyle(font: .ProductCompareFeatValueFont, textColor: UIColor.mdGreyishBrown)
    }
    
    public static var ProductGridNameStyle: Style{
        return Style{ label in
            label.font = .ProductGridNameFont
            label.textColor = UIColor.mdGreyishBrown
        }
    }
    
    public static var ProductGridBestPriceStyle: Style{
        return Style{ label in
            label.font = .ProductGridBestPriceFont
            label.textColor = Theme.current.accentColor
        }
    }
    
    public static var ProductGridCompareStyle: Style{
        return Style.GenericLabelStyle(font: .ProductGridCompareFont, textColor: .white)
    }
    
   public static var ProductGridCancelStyle: Style{
        return Style.GenericLabelStyle(font: .ProductGridCancelCompareFont, textColor: .white)
    }
    
    public static var ProductGridDiscountStyle: Style{
        return Style.GenericLabelStyle(font: .ProductGridDiscountFont, textColor: .white)
    }
    
    public static var ProductGridOtherPriceStyle: Style{
        return Style { label in
            label.font = .ProductGridOtherPriceFont
            label.textColor = UIColor.mdWarmGrey
        }
    }
    public static var SubCategoryNameStyle: Style{
        return Style{ label in
            label.font = .SubCategoryNameFont
            label.textColor = Theme.current.accentColor
        }
    }
    
    public static var CategoryTitleStyle: Style {
        return Style { label in
            label.font = .CategoryTitleFont
            label.textColor = Theme.current.accentColor
        }
    }
    
   public static var CategorySubTitleStyle: Style {
        return Style { label in
            label.font = .CategorySubtitleFont
        }
    }
    
    public static var CategoryNameStyle: Style {
        return Style { label in
            label.font = .CategoryNormalNameFont
            label.textColor = .white
            label.backgroundColor = Theme.current.mainColor.withAlphaComponent(0.9)
            
        }
    }
    
    
    public static var LoginRecoverPasswordStyle: Style{
        return Style {label in
            label.font = .recoverPasswordtFont
            label.textColor = .mdWarmGrey
        }
    }
    
    public static var LoginTitleStyle: Style{
        return Style {label in
            label.font = .LoginTitleFont
            label.textColor = Theme.current.accentColor
        }
    }
    
    public static var LoginParamNameStyle: Style{
        return Style {label in
            label.font = .LoginParamNameFont
            label.textColor = Theme.current.accentColor
        }
    }
    
    
    
}
