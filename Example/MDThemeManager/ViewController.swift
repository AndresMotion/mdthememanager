//
//  ViewController.swift
//  MDThemeManager
//
//  Created by Andres Rivas on 02/20/2018.
//  Copyright (c) 2018 Andres Rivas. All rights reserved.
//

import UIKit
import MDThemeManager

class ViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
    
        let testLabel = BestPriceLabel.init(frame: CGRect.init(x: 20, y: 40, width: 260, height: 90))
        testLabel.text = "Esto es una pruebita"
        
        testLabel.apply(styles: .ProductDetailBestPriceValueStyle)
       
        let testIcon = SearchIconImageView.init(frame: CGRect.init(x: 60, y: testLabel.frame.maxY + 20, width: 90, height: 90))
        testIcon.apply(styles: Style.GenerateNavBarIcon(iconImage: UIImage.navBarSearchIcon))
        
        self.view.addSubview(testIcon)
//        testLabel.textColor = UIColor.testGetMainColor()
    
        self.view.addSubview(testLabel)
    
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

}

