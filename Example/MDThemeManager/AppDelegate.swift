//
//  AppDelegate.swift
//  MDThemeManager
//
//  Created by Andres Rivas on 02/20/2018.
//  Copyright (c) 2018 Andres Rivas. All rights reserved.
//

import UIKit
import MDThemeManager



@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {

    var window: UIWindow?


    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        
        let testColor = MDColorObject()
        testColor.name = ThemeColorsId.mainColorName
        testColor.hex_value = "#FFFF00"

        var testArrayColor = Array<MDColorObject>()
        testArrayColor.append(testColor)
        
        let testImage = MDImageObject()
        testImage.name = ThemeImagesId.logoName
        testImage.remote_url_value = "https://s3-sa-east-1.amazonaws.com/projectxparis/interfaz/logo.png"
        
        var testArrayImage = Array<MDImageObject>()
        testArrayImage.append(testImage)
        
        let testFont = MDFontObject()
        testFont.name = ThemeFontsId.regularFontName
        testFont.type = "Regular"
        testFont.file_name = "calibri.ttf"
        testFont.font_family_name = "Arial"
        testFont.remote_url_value = "https://github.com/jondot/dotfiles/raw/master/.fonts/calibri.ttf"
        
        let testFontTwo = MDFontObject()
        testFontTwo.name = ThemeFontsId.boldFontName
        testFontTwo.type = "Bold"
        testFontTwo.file_name = "calibrib.ttf"
        testFontTwo.font_family_name = "Arial"
        testFontTwo.remote_url_value = "https://s3-sa-east-1.amazonaws.com/projectxsodimac/interfaz/Omega+Ruby.ttf"
        
        
        
        var testFontArray = Array<MDFontObject>()
        testFontArray.append(testFont)
        testFontArray.append(testFontTwo)
    
//        Theme.current.newTheme(paletaColores: testArrayColor, assetsImagenes: testArrayImage, fontAssets: testFontArray){
//           semaphore.signal()
//        }
        
        
        if Theme.current == Theme.custom{
            print("Cargando custom")
            Theme.current.loadTheme {
                Theme.current.apply()
            }
        } else{
            print("Creando nuevo")
            Theme.custom.newTheme(paletaColores: testArrayColor, assetsImagenes: testArrayImage, fontAssets: testFontArray, completion: {
                Theme.custom.apply()
            })
        }
        
        
  
        return true
    }

    func applicationWillResignActive(_ application: UIApplication) {
        // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
        // Use this method to pause ongoing tasks, disable timers, and throttle down OpenGL ES frame rates. Games should use this method to pause the game.
    }

    func applicationDidEnterBackground(_ application: UIApplication) {
        // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
        // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
    }

    func applicationWillEnterForeground(_ application: UIApplication) {
        // Called as part of the transition from the background to the inactive state; here you can undo many of the changes made on entering the background.
    }

    func applicationDidBecomeActive(_ application: UIApplication) {
        // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
    }

    func applicationWillTerminate(_ application: UIApplication) {
        // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
    }


}

